# gpu-blocker

When working on a remote multi-GPU server with colleagues or students, it is
convenient to have a software solution to allocate individual GPUs on a per-user
basis.
**gpu-blocker** is a script that provides a command-line interface (CLI) for
per-user GPU booking.

Features:
* No external calendar necessary.
* Collisions are detected automatically. A GPU cannot be overbooked by another user.
* Multiple GPUs can be booked at once using an easy CLI syntax.
* Forbid users to use GPUs that are not booked, by killing processes running on unallocated GPUs.

Planned features:
* iCal export and upload.
* Checkout the [issue tracker](https://codeberg.org/henrykrumb/gpu-blocker/issues) to get an overview of planned features.


![](screenshot.png)



## Usage

First, check how many GPUs can be accessed by the system:

```bash
./blockgpu gpus
```

Next, we'll check the schedule of one of the GPUs:

```bash
./blockgpu free --year YEAR --month MONTH GPU
```

You will be presented a colorful calendar.
If you have found a slot that suits you, you can block it by the 'block'
subcommand:

```bash
./blockgpu block GPU START_DATE END_DATE
```

You can provide both dates in "DD.MM.YYYY" or "YYYY/MM/DD" format.
To keep track of your own reservations, see:

```bash
./blockgpu my
```


## Installation

Install the script to:

```bash
sudo cp blockgpu /usr/bin/
```

make sure dependencies are available system-wide:

```bash
sudo pip install click nvsmi torch
```

Optional, but recommended:
Install timer and service:
```bash
sudo cp blockgpu.service /etc/systemd/system
sudo cp blockgpu.timer /etc/systemd/systemsudo systemctl start blockgpu.timer
```

