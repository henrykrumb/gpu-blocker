#!/usr/bin/env python3
import calendar
import copy
import curses
import datetime
import json
import os
import signal

import click
import nvsmi
import torch


date_formats = [
    '%d.%m.%Y',
    '%Y/%m/%d'
]

file_path = os.path.dirname(__file__)
reservations_file = os.path.join(file_path, 'reservations.json')


@click.group()
def cli():
    pass


def num_gpus():
    return torch.cuda.device_count()


@cli.command(help='Show total number of available GPUs')
def gpus():
    print(num_gpus())


def get_user():
    return os.getlogin()


def str2gpu(s):
    # TODO match regex to check if format is right
    tokens = []
    for token in s.split(','):
        newtokens = token.split('-')
        if len(newtokens) == 1:
            tokens.append(token)
        else:
            for i, t in enumerate(newtokens):
                if i == 0:
                    continue
                start = int(newtokens[i-1])
                end = int(t)
                r = list(range(start, end + 1))
                tokens.extend(r)
    gpus = sorted(list(set([int(token) for token in tokens])))
    return gpus


class Reservation:
    name: str
    gpu: int
    start: datetime.datetime
    end: datetime.datetime

    def __init__(self, name, gpu, start, end):
        assert start <= end
        self.name = name
        self.gpu = gpu
        self.start = start
        self.end = end

    def intersects(self, name, gpu, start, end):
        if name == self.name:
            return False
        if gpu != self.gpu:
            return False
        if start > self.end:
            return False
        if end < self.start:
            return False
        return True

    @staticmethod
    def from_dict(d):
        name = str(d['name'])
        gpu = int(d['gpu'])
        start = datetime.datetime.fromisoformat(d['start'])
        end = datetime.datetime.fromisoformat(d['end'])
        return Reservation(name, gpu, start, end)

    def to_dict(self):
        return {
            'name': self.name,
            'gpu': self.gpu,
            'start': self.start.isoformat(' '),
            'end': self.end.isoformat(' ')
        }

    def __str__(self):
        start = self.start.strftime('%d.%m.%Y')
        end = self.end.strftime('%d.%m.%Y')
        return f'{self.name} gpu {self.gpu} | {start} - {end}'

    def __in__(self, x):
        if isinstance(x, datetime.datetime):
            return x.date() >= self.start.date() and x.date() <= self.end.date()
        return NotImplemented

    def __eq__(self, other):
        if isinstance(other, Reservation):
            return self.name == other.name and self.gpu == other.gpu and \
                    self.start.date() == other.start.date() and self.end.date() == other.end.date()
        return NotImplemented


def load_reservations():
    if not os.path.isfile(reservations_file):
        return []
    else:
        with open(reservations_file, 'r') as f:
            reservation_list = json.load(f)
        return [
            Reservation.from_dict(r)
            for r in reservation_list
        ]


def save_reservations(reservations):
    reservation_list = [r.to_dict() for r in reservations]
    with open(reservations_file, 'w') as f:
        json.dump(reservation_list, f)


def add_to_reservation_list(reservation, reservation_list):
    # invariant: reservation does not intersect
    for i, r in enumerate(reservation_list):
        # see if there is already a reservation by same user on same GPU in
        # same timeline
        if reservation.name != r.name:
            continue
        if reservation.gpu != r.gpu:
            continue
        if reservation.start > r.end or reservation.end < r.start:
            continue
        
        start = None
        end = None
        if reservation.start < r.start:
            start = reservation.start
        else:
            start = r.start
        if reservation.end > r.end:
            end = reservation.end
        else:
            end = r.end
        reservation_list[i].start = start
        reservation_list[i].end = end
        return reservation_list
    reservation_list.append(reservation)
    return reservation_list


def make_reservation(name, gpu_list, start, end):
    reservations = load_reservations()
    intersection = False
    for gpu in gpu_list:
        for reservation in reservations:
            if reservation.intersects(name, gpu, start, end):
                click.secho(f'Intersection detected.', fg='red')
                click.secho(f'    {name}, GPU{gpu}, {start} - {end}')
                intersection = True
    if intersection:
        click.secho(f'Intersection(s) occured. Could not block GPU.', fg='yellow')
    else:
        for gpu in gpu_list:
            reservations = add_to_reservation_list(Reservation(name, gpu, start, end), reservations)
        save_reservations(reservations)
        return True
    return False


@cli.command(help='Reserve a GPU for a certain period')
@click.argument('gpu', type=str)
@click.argument('start', type=click.DateTime(formats=date_formats))
@click.argument('end', type=click.DateTime(formats=date_formats))
def block(gpu, start, end):
    gpu_list = str2gpu(gpu)
    for g in gpu_list:
        if g >= num_gpus():
            click.secho(f'Invalid GPU index {g}.', fg='red')
            click.secho(f'Only {num_gpus()} GPUs are available.')
            return
    make_reservation(get_user(), gpu_list, start, end)


def my_reservations():
    reservations = load_reservations()
    return [
        reservation for reservation in reservations
        if reservation.name == get_user()
    ]


def other_reservations():
    """
    Return reservations by other users
    """
    reservations = load_reservations()
    return [
        reservation for reservation in reservations
        if reservation.name != get_user()
    ]


def date_in_reservation_list(date, gpu_list, reservations):
    for reservation in reservations:
        if reservation.__in__(date) and reservation.gpu in gpu_list:
            return True


def reservations_for_date(date, reservations):
    return [r for r in reservations if r.__in__(date)]


@cli.command(help="List my own reservations")
def my():
    for reservation in my_reservations():
        click.secho(str(reservation))


@cli.command(help='Unblock GPUs interactively')
def unblock():
    reservations = load_reservations()
    my = my_reservations()
    for i, reservation in enumerate(my):
        click.secho(f'{i:3d}   {str(reservation)}')
    click.secho('')
    click.secho('Please select the slot(s) you would like to unblock.')
    click.secho('Separate multiple choices by space.')
    choice = input('Your choice [q]> ')
    choice = choice.strip()
    if choice == 'q' or not choice:
        return
    choice = choice.split(' ')
    choice = [token.strip() for token in choice if token.strip()]
    try:
        indices = list(set([int(c) for c in choice]))
    except ValueError:
        click.secho(f'Invalid value in input "{choice}".', fg='red')
        return
    for index in indices:
        if index >= len(my) or index < 0:
            click.secho(f'Invalid index {index}.', fg='red')
            return
        r = my[index]
        reservations.remove(r)
    save_reservations(reservations)


@cli.command(help='See on which days a GPU is still free')
@click.option('--month', '-m', default=datetime.datetime.now().month)
@click.option('--year', '-y', default=datetime.datetime.now().year)
@click.argument('gpu', type=str)
def free(month, year, gpu):
    gpu_list = str2gpu(gpu)

    for gpu in gpu_list:
        if gpu >= num_gpus():
            click.secho(f'Invalid GPU index {gpu}.', fg='red')
            click.secho(f'Only {num_gpus()} GPUs are available.')
            return
    my = my_reservations()
    other = other_reservations()
    if len(gpu_list) == 1:
        click.secho(f'Schedule for GPU {gpu_list[0]}:')
    else:
        slist = ','.join([str(g) for g in gpu_list])
        click.secho(f'Schedule for GPUs {slist}:')
    click.secho('')
    cal = calendar.monthcalendar(year, month)
    click.secho(' Mo Tu We Th Fr Sa Su')
    for week in cal:
        click.secho(' ', nl=False)
        for day in week:
            if day == 0:
                click.secho(f'   ', nl=False)
            else:
                date = datetime.datetime(year=year, month=month, day=day)
                # highlight my booking as blue, other as red
                bg = None
                if date_in_reservation_list(date, gpu_list, my):
                    bg = 'blue'
                elif date_in_reservation_list(date, gpu_list, other):
                    bg = 'red'
                click.secho(f'{day:2d} ', bg=bg, nl=False)
        click.secho('')
    click.secho('')
    click.secho('  ', bg='blue', nl=False)
    click.secho(' blocked by you')
    click.secho('  ', bg='red', nl=False)
    click.secho(' blocked by other users')


def too_old(reservation):
    now = datetime.datetime.now()
    delta = now - reservation.end
    # check if older than ~3 months
    return delta.days >= 90


@cli.command(help='Clean up history of past reservations')
def clean():
    reservations = load_reservations()
    old_length = len(reservations)
    reservations = [r for r in reservations if not too_old(r)]
    new_length = len(reservations)
    click.secho(f'Purged {old_length - new_length} old reservations.')
    save_reservations(reservations)


@cli.command(help='List GPUs you have blocked for today')
def blocked():
    my = my_reservations()
    today = datetime.datetime.now()
    gpus = []
    for reservation in my:
        if today >= reservation.start and today <= reservation.end:
            gpus.append(str(reservation.gpu))
    print(','.join(gpus))


def process_owner(pid):
    with open(f'/proc/{pid}/status', 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line.startswith('Uid:'):
                uid = int(line.split()[1])
                return pwd.getpwuid(uid).pw_name
    return ''


@cli.command(help='Kill processes that use GPU without permission')
@click.option('-r', '--rigorous', is_flag=True, help='Even kill processes if GPU is not allocated by anyone')
def kill(rigorous):
    date = datetime.datetime.now()
    reservations = load_reservations()
    reservations = reservations_for_date(date, reservations)
    pids = { process.gpu_id: process.pid for process in nvsmi.get_gpu_processes() }
    for gpu, pid in pids.items():
        ok = False
        for reservation in reservations:
            if reservation.gpu != gpu:
                continue
            owner = process_owner(pid)
            if owner == reservation.name:
                ok = True
                continue
            # at this point, GPU is used by someone else
            # (excluding root also prevents Xorg being killed)
            if owner != 'root':
                os.kill(pid, signal.SIGKILL)
            ok = True
        # there is a process which uses GPU without any reservation being made
        if not ok and rigorous and owner != 'root':
            os.kill(pid, signal.SIGKILL)


def delete_slot(user, cursor_date, all_reservations):
    all_reservations = [
        r for r in all_reservations
        if not (r.__in__(cursor_date) and r.name == user)
    ]
    save_reservations(all_reservations)


@cli.command(help='Interactively block GPU')
def iblock():    
    def main(stdscr):
        gpu_list = list(range(num_gpus()))
        offset_x = 3
        offset_y = 1
        my = my_reservations()
        other = other_reservations()
        all_reservations = load_reservations()

        curses.curs_set(0)
        stdscr.keypad(True)
        stdscr.timeout(10)
        curses.start_color()
        stdscr.clear()
        stdscr.refresh()

        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_BLUE)
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_RED)
        curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_MAGENTA)
        curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_WHITE)

        gpu_y = 0
        selected_gpus = []
        selected_start = None
        selected_end = None

        cursor_date = datetime.datetime.now()

        while True:
            # show interactive calendar widget
            year = cursor_date.year
            month = cursor_date.month
            cal = calendar.monthcalendar(year, month)
            
            for y in range(7):
                stdscr.addstr(offset_y + y + 2, offset_x, ' ' * 4 * 7)
            stdscr.addstr(offset_y, 3, f'{month:02d} / {year}')
            stdscr.addstr(offset_y + 1, 3, 'Mon Tue Wed Thu Fri Sat Sun', curses.A_BOLD)
        
            for row, week in enumerate(cal):
                for col, day in enumerate(week):
                    if day == 0:
                        stdscr.addstr(row + offset_y + 2, col * 4 + offset_x, '   ')
                    else:
                        attr = 0
                        prefix = ' '
                        suffix = ' '
                        date = datetime.datetime(year=year, month=month, day=day)
                        if date_in_reservation_list(date, gpu_list, my) and date_in_reservation_list(date, gpu_list, other):
                            attr |= curses.color_pair(3)
                        elif date_in_reservation_list(date, gpu_list, my):
                            attr |= curses.color_pair(1)
                        elif date_in_reservation_list(date, gpu_list, my):
                            attr |= curses.color_pair(2)
                        if selected_start is not None:
                            if (date <= cursor_date and date >= selected_start) or \
                                (date <= selected_start and date >= cursor_date):
                                attr |= curses.A_UNDERLINE
                        if cursor_date.date() == date.date():
                            attr |= curses.A_BOLD
                            prefix = '['
                            suffix = ']'
                        stdscr.addstr(row + offset_y + 2, col * 4 + offset_x, f'{prefix}{day:2d}{suffix}', attr)

            # show GPU list
            for i, gpu in enumerate(gpu_list):
                attr = 0
                status = 'x' if gpu in selected_gpus else ' '
                if selected_start is None:
                    attr = curses.color_pair(4) if i == gpu_y else 0
                    prefix = '<' if i == gpu_y else ' '
                    suffix = '>' if i == gpu_y else ' '
                stdscr.addstr(i + offset_y + 1, offset_x + 4 * 7 + 7, f'{prefix} GPU {gpu} ({status}) {suffix}', attr)

            # show info about currently hovered calendar item
            date_reservations = reservations_for_date(cursor_date, my)
            my_date_gpus = ','.join([str(r.gpu) for r in date_reservations])
            date_reservations = reservations_for_date(cursor_date, other)
            other_date_gpus = ','.join([str(r.gpu) for r in date_reservations])

            # clear lines the cheap way :)
            stdscr.addstr(offset_y + 10, offset_x, f'GPUs booked by me:                      ')
            stdscr.addstr(offset_y + 11, offset_x, f'GPUs booked by others:                  ')

            stdscr.addstr(offset_y + 10, offset_x, f'GPUs booked by me:     {my_date_gpus}')
            stdscr.addstr(offset_y + 11, offset_x, f'GPUs booked by others: {other_date_gpus}')

            # show controls
            stdscr.addstr(offset_y + 15, offset_x, 'W,A,S,D: Navigate calendar')
            stdscr.addstr(offset_y + 16, offset_x, 'Up, Down, Left, Right: Select GPUs')
            stdscr.addstr(offset_y + 17, offset_x, 'Space: Select range start/end')
            stdscr.addstr(offset_y + 18, offset_x, 'X: Remove slot')
            stdscr.addstr(offset_y + 19, offset_x, 'C: Cancel range selection')
            stdscr.addstr(offset_y + 20, offset_x, 'Q: Quit')

            stdscr.refresh()

            # process user input
            c = stdscr.getch()
            if c == ord('q'):
                return
            elif c == ord('a'):
                cursor_date -= datetime.timedelta(days=1)
            elif c == ord('d'):
                cursor_date += datetime.timedelta(days=1)
            elif c == ord('w'):
                cursor_date -= datetime.timedelta(days=7)
            elif c == ord('s'):
                cursor_date += datetime.timedelta(days=7)
            # select GPU from menu
            elif c == curses.KEY_UP and selected_start is None:
                gpu_y -= 1
                if gpu_y < 0:
                    gpu_y = 0
            elif c == curses.KEY_DOWN and selected_start is None:
                gpu_y += 1
                if gpu_y >= len(gpu_list):
                    gpu_y -= 1
            # toggle GPU on/off
            elif c == curses.KEY_LEFT or c == curses.KEY_RIGHT and selected_start is None:
                if gpu_list[gpu_y] in selected_gpus:
                    selected_gpus.remove(gpu_y)
                else:
                    selected_gpus.append(gpu_y)
                stdscr.addstr(offset_y + 8, offset_x, ' ' * 30)
            # select start date for reservation
            elif c == ord(' '):
                if selected_start is None:
                    if selected_gpus:
                        selected_start = copy.copy(cursor_date)
                    else:
                        stdscr.addstr(8, offset_x, 'Please select one or more GPUs first.')
                else:
                    selected_end = copy.copy(cursor_date)
                    if selected_end < selected_start:
                        temp = selected_end
                        selected_end = selected_start
                        selected_start = temp
                    if make_reservation(get_user(), selected_gpus, selected_start, selected_end):
                        my = my_reservations()
                        other = other_reservations()
                        all_reservations = load_reservations()
                    else:
                        stdscr.addstr(offset_y + 8, offset_x, 'Intersection detected.')
                    selected_start = None
                    selected_end = None
            # cancel selection
            elif c == ord('c'):
                selected_start = None
            elif c == ord('x'):
                all_reservations = delete_slot(get_user(), cursor_date, all_reservations)
                my = my_reservations()
                other = other_reservations()
                all_reservations = load_reservations()
    curses.wrapper(main)


if __name__ == '__main__':
    cli()
